//no 1

var i = 0;
var k = 22;

console.log("LOOPING PERTAMA")

while(i < 20){
    i+=2;
    console.log(i + "- I love coding")
}

console.log("LOOPING KEDUA")

while(k > 0){
    k-=2;
    console.log(k + "- I will become a mobile developer")
}

console.log("\n")

//no 2

var i = 1;

for(var k =20; i<=k; i++){
   if(i%3==0 && i%2==1  ){
       console.log(i + "- I Love coding")
   }else if(i%2==1){
       console.log(i + "- santai")
   }else{
       console.log(i + "- Berkualitas")
   }
}

console.log("\n")

//no 3
for(var i = 1; i<=4; i++){
    for(var j = 1; j<=8; j++){
        process.stdout.write("#")
    }console.log()
}

console.log("\n")

//no4
for(var i = 1; i<=7; i++){
    for(var j = 1; j<=i; j++){
        process.stdout.write("#")
    }console.log()
}

console.log("\n")

//no5

var papan = " ";
for(var i = 1; i <= 8; i++){
    for(var j = 1; j<=4; j++){
        if(i%2==0){
            process.stdout.write("# ")
        }else{
            process.stdout.write(" #")
        }
    }console.log()
}